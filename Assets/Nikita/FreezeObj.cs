﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeObj : MonoBehaviour
{
    private List<Rigidbody> Object_List = new List<Rigidbody>();
    void Update()
    {
        //This removes all destroyed Rigidbodies
        Object_List.RemoveAll(rb => !rb); 
        if (Input.GetKeyDown(KeyCode.E))
        {
            foreach (Rigidbody rb in Object_List)
            {
                rb.isKinematic = !rb.isKinematic;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Spawned")
        {
            Object_List.Add(other.GetComponent<Rigidbody>());
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Spawned")
        {
            Object_List.Remove(other.GetComponent<Rigidbody>());
        }
            
    }
}
